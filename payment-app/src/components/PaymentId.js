import React from "react";

const PaymentId = (props) => <td>{props.id}</td>;

export default PaymentId;
