import Payment from "./Payment";
import React from "react";
const PaymentTableBody = ({ payments, loadList }) => {
	return (
		loadList && (
			<tbody>
				{payments.map((payment) => (
					<Payment
						key={payment.id}
						id={payment.id}
						date={payment.paymentDate}
						amount={payment.amount}
						type={payment.type}
						custId={payment.custId}
					/>
				))}
			</tbody>
		)
	);
};
export default PaymentTableBody;
