import React from "react";
const PaymentHeader = () => (
	<thead>
		<tr>
			<th scope="col">PaymentId</th>
			<th scope="col">Payment Date</th>
			<th scope="col">Payment Amount</th>
			<th scope="col">Payment Type</th>
			<th scope="col">Customer Id</th>
		</tr>
	</thead>
);

export default PaymentHeader;
