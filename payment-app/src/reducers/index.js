import { combineReducers } from "redux";
import paymentReducer from "./paymentReducer";
import errorReducer from "./errorReducer";
import statusReducer from "./statusReducer";
import addPaymentReducer from "./addPaymentReducer";

const rootReducer = combineReducers({
	payment: paymentReducer,
	errors: errorReducer,
	status: statusReducer,
	addPayment: addPaymentReducer,
});
export default rootReducer;
