const initialState = {};

const addPaymentReducer = (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in StatusReducer`);
	switch (action.type) {
		case "ADD_PAYMENT_BEGIN":
			return { ...state, loading: true, error: null };
		case "ADD_PAYMENT_SUCCESS":
			return { ...state, loading: false, success: action.payload };
		case "ADD_PAYMENT_FAILURE":
			return { ...state, loading: false, error: action.payload };
		default:
			return state;
	}
};
export default addPaymentReducer;
